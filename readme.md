This app is created for Monese for their task. 

There are some things to mention;  
	- No tests are written because I didn't want to include Selenium into my project for this.  
	- Technologies used are pretty old and I know some of the annotations are deprecated. However my job computer does not have newer Java versions, so I couldn't even use newer annotations.  
	- This application took approximately 2.42 hours to write. I wanted to keep it in the given time limit so I'm aware there are some bugs :)  
	  
Technologies used;  
	- Wildfly 12.0 used for the application server.  
	- MySQL for the database  
	
How to use;  
	- First you need to update persistence.xml according to your settings  
	- After you deploy project into the application server, it opens a registration page. The pages are not accessible if no logins are detected.  
	- After your login there are some links on every page to redirect. There is also a bug here so please don't click on the same links over and over :)  
	- You can use manuel routing (such as user/depositmoney.xhtml, user/eft.xhtml, user/mybankaccount.xhtml)  
	- Depositmoney allows you to deposit money into an account, eft page lets you to send money to another existing account using IBAN. Mybankaccount page shows the initial balance of the user you have logged on with.  

With this readme I think I'm done with my 3 hours of time limit. Hope this suits your needs! Thank you in advance  