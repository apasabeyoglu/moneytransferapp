package atil.pasabeyoglu.mbeans;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import atil.pasabeyoglu.services.UserService;

@ManagedBean
@SessionScoped
public class RegisterBean {
	
	private String name;
	private String lastName;
	private String email;
	private String password;
	private String iban;

	@EJB
	private UserService userService;
	
	public RegisterBean() {
	}

	public RegisterBean(String name, String lastName, String email, String password, String iban) {
		super();
		this.name = name;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
		this.iban=iban;
	}

	public String save() {

		userService.register(this.name, this.lastName, this.email, this.password,this.iban);
		return "login";
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public String getIban() {
		return iban;
	}

	public void setIban(String iban) {
		this.iban = iban;
	}
	
}
