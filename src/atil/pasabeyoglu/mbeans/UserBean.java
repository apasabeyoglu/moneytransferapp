package atil.pasabeyoglu.mbeans;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import atil.pasabeyoglu.services.UserService;

@ManagedBean
@SessionScoped
public class UserBean {
	
	private int id;
	private String senderIban;
	private String receiverIban;
	private double amount;
	private String email;
	
	@EJB
	private UserService userService;
	
	@ManagedProperty("#{loginBean}")
	private LoginBean loginBean;
	
	public void sendMoney() {
		userService.sendMoney(receiverIban, loginBean.getUser().getIban(), amount);
	}
	
	public void deposit() {
		userService.depositMoney(amount, loginBean.getUser().getIban());
	}
	
	public void eft() {
		userService.eft(amount, receiverIban, loginBean.getUser().getIban());
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSenderIban() {
		return senderIban;
	}

	public void setSenderIban(String senderIban) {
		this.senderIban = senderIban;
	}

	public String getReceiverIban() {
		return receiverIban;
	}

	public void setReceiverIban(String receiverIban) {
		this.receiverIban = receiverIban;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public LoginBean getLoginBean() {
		return loginBean;
	}

	public void setLoginBean(LoginBean loginBean) {
		this.loginBean = loginBean;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getEmail() {
		return email;
	}

}
