package atil.pasabeyoglu.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import atil.pasabeyoglu.domain.Transaction;
import atil.pasabeyoglu.domain.User;

@Path("UserService")
@Stateless
public class UserService {
	
	@PersistenceContext
	private EntityManager em;
	
	public void depositMoney(double amount, String iban) {

		User user=em.createQuery("select u from User u where u.iban=:iban",User.class)
				.setParameter("iban", iban).getSingleResult();
		user.setBalance(user.getBalance()+amount);
		em.merge(user);

	}
	
	public void sendMoney (String receiverIban, String senderIban, double amount) {
		User user=em.createQuery("select u from User u where u.iban=:iban",User.class)
				.setParameter("iban", receiverIban).getSingleResult();
		user.setBalance(user.getBalance()+amount);
		em.merge(user);
		
		User user2=em.createQuery("select u from User u where u.iban=:iban",User.class)
				.setParameter("iban", senderIban).getSingleResult();
		user2.setBalance(user2.getBalance()-amount);
		em.merge(user2);
		
		saveTransaction(user2.getName(), user2.getLastName(), senderIban, receiverIban, -amount, user2.getBalance());

	}	
	
	public void register(String name, String lastName, String email, String password,String iban) {
		User user = new User(name, lastName, email, password, iban, 0);
		em.persist(user);
	}
	
	public void saveTransaction (String name, String lastName, String iban, String receiverIban, double transactionAmount, double balance) {
		Transaction transaction = new Transaction(name, lastName, iban, receiverIban, transactionAmount, balance);
		em.persist(transaction);
	}
	
	@GET
	@Path("eft")
	@Produces(MediaType.APPLICATION_JSON)
	public void eft(double amount, String receiverIban, String senderIban) {
		this.sendMoney(receiverIban,senderIban,amount);
	}
	
	public User getUser(String email, String password) {
		
		List<User> users = em.createQuery
				("select u from User u where u.email=:email and u.password=:password", User.class)
		.setParameter("email", email)
		.setParameter("password", password)
		.getResultList();

		if(users.size()==1) {
			return users.get(0);
		}	
		return null;
	}

}
