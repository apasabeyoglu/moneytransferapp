package jaxrsclient;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

public class MainClass {

	public static void main(String[] args) {
		
		try {
			
			
			URL url = new URL("http://localhost:8080/JAX-RS-Service/rest"
					+ "/UserService/eft");
			InputStream is = url.openConnection().getInputStream();
			BufferedReader reader = new BufferedReader(
					new InputStreamReader( is )  );
			String line = null;
			while( ( line = reader.readLine() ) != null )  {
			       System.out.println(line);
			}
			
			
			reader.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
	
